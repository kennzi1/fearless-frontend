window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';

  const response = await fetch(url);

  if (response.ok) {
      const data = await response.json();
      console.log(data);

      const selectTag = document.getElementById('conference');
      for (let conference of data.conferences) {
          const option = document.createElement('option')
          option.innerHTML = conference.name;
          option.value = conference.id;
          selectTag.appendChild(option)
      }

      const selectElement = document.getElementById('conference');
      selectElement.addEventListener('change', (event) => {
        const urlV = event.target.value;
        console.log("value: ", urlV)


        const formTag = document.getElementById('create-presentation-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            const presentationUrl = `http://localhost:8000/api/conferences/${urlV}/presentations/`;
            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                },
            };
            const response = await fetch(presentationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newPresentation = await response.json();
                console.log(newPresentation);
            }
        })
      });
  }
});
