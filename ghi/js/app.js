function createCard(name, description, pictureUrl, startDate, endDate, location) {
  return `
      <div class="col">
        <div class="card shadow shadow-2-strong">
          <img src=${pictureUrl} class="card-img-top">
          <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
          </div>
          <div class="card-footer">${startDate} - ${endDate}</div>
        </div>
      </div>
  `;
}

function showError() {
  return `
    <div class="alert alert-warning" role="alert">
      Response not ok
    </div>
  `;
}

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw new Error('Repsonse not ok');
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            console.log(details);
            const name = details.conference.name;
            const description = details.conference.description;
            const location = details.conference.location.name;
            const pictureUrl = details.conference.location.picture_url;
            const start = details.conference.starts;
            const end = details.conference.ends;
            const startDate = getDate(start);
            const endDate = getDate(end);
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
          }
        }

      }
    } catch (error) {
      console.error('error', error);
      const htmlError = showError();
      const container = document.querySelector('.container');
      container.innerHTML = htmlError;
    }

});

function getDate(dateInfo) {
  const date = new Date(dateInfo);
  return date.toLocaleDateString();
}
