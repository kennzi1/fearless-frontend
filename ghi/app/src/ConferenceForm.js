import React from 'react';

class ConferenceForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      name: '',
      starts: '',
      ends: '',
      maxPresentations: '',
      maxAttendees: '',
      description: '',
      locations: []
    };
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleStartDateChange = this.handleStartDateChange.bind(this);
    this.handleEndDateChange = this.handleEndDateChange.bind(this);
    this.handleMaxPresentChange = this.handleMaxPresentChange.bind(this);
    this.handleMaxAttendeeChange = this.handleMaxAttendeeChange.bind(this);
    this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    this.handleLocationChange = this.handleLocationChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleNameChange(event) {
    const value = event.target.value;
    this.setState({name: value});
  }

  handleStartDateChange(event) {
    const value = event.target.value;
    this.setState({starts: value});
  }

  handleEndDateChange(event) {
    const value = event.target.value;
    this.setState({ends: value});
  }

  handleMaxPresentChange(event) {
    const value = event.target.value;
    this.setState({maxPresentations: value});
  }

  handleMaxAttendeeChange(event) {
    const value = event.target.value;
    this.setState({maxAttendees: value});
  }

  handleDescriptionChange(event) {
    const value = event.target.value;
    this.setState({description: value});
  }

  handleLocationChange(event) {
    const value = event.target.value;
    this.setState({location: value});
  }

  async handleSubmit(event) {
    event.preventDefault();
    const data = {...this.state};
    data.max_presentations = data.maxPresentations;
    data.max_attendees = data.maxAttendees;
    delete data.maxPresentations;
    delete data.maxAttendees;
    delete data.locations;
    console.log(data);

    const conferenceUrl = 'http://localhost:8000/api/conferences/';
    const fetchConfig= {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      }
    };
    const response = await fetch(conferenceUrl, fetchConfig);
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);

      const cleared = {
        name: '',
        starts: '',
        ends: '',
        maxPresentations: '',
        maxAttendees: '',
        location: '',
        description: '',
      };
      this.setState(cleared);
    }
  }
  async componentDidMount() {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({locations: data.locations})
      console.log(data)
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartDateChange} required type="date" name="starts" id="starts" min="2022-10-01" className="form-control" />
                <label htmlFor="starts">Start date:</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndDateChange} required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">End date:</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentChange} required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations:</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeeChange} required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees:</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea className="form-control" id="description" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                      <option key={location.name} value={location.id}>
                        {location.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ConferenceForm;
